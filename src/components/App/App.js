// @flow

import React from "react";
import styles from "./App.module.css";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../../store/sagas";
import rootReducer from "../../store/reducers";
import UsersPageContainer from "../UsersPage/UsersPageContainer";
import UserContainer from "../User/UserContainer";
import CreateEditUserContainer from "../CreateEditUser/CreateEditUserContainer";
import NoMatch from "../NoMatch/NoMatch";

const sagaMiddleware = createSagaMiddleware();
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));
sagaMiddleware.run(rootSaga);

function App() {
  function renderCreateEditUserContainer(props, isNew) {
    return (
      <CreateEditUserContainer
        isNew={isNew}
        match={props.match}
        history={props.history}
      />
    );
  }

  return (
    <Provider store={store}>
      <div className={styles.container}>
        <Router>
          <Switch>
            {/*Страница со списком пользователей*/}
            <Route exact path="/" component={UsersPageContainer} />
            {/*Страница со списком пользователей на выбраной странице*/}
            <Route path="/page/:page" component={UsersPageContainer} />
            {/*Страница создания пользователя*/}
            <Route
              exact
              path="/users/new"
              render={props => renderCreateEditUserContainer(props, true)}
            />
            {/*Страница с информацией о конкретном пользователе*/}
            <Route exact path="/users/:id" component={UserContainer} />
            {/*Страница редактирования пользователя*/}
            <Route
              path="/users/:id/edit"
              render={props => renderCreateEditUserContainer(props, false)}
            />
            {/*Страница отсутсвует*/}
            <Route path="*">
              <NoMatch />
            </Route>
          </Switch>
        </Router>
      </div>
    </Provider>
  );
}

export default App;
