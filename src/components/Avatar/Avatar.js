// @flow

import React from "react";
import emptyAvatar from "../../assets/empty-avatar.jpg";
import styles from "./Avatar.module.css";

type AvatarProps = {
  href: string,
  width: string,
  height: string
};

export default function Avatar(props: AvatarProps) {
  function getSrc() {
    // Если props.href присутсвует и не пустой
    if (props.href && props.href !== "") {
      // то возвращаем родительский href
      return props.href;
    } else {
      // или возвращаем заглушку
      return emptyAvatar;
    }
  }

  return (
    <img
      className={styles.image}
      src={getSrc()}
      width={props.width || 100}
      height={props.height || 100}
      alt="avatar"
    />
  );
}
