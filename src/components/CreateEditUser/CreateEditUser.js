// @flow

import React, { Component } from "react";
import PropTypes from "prop-types";
import Avatar from "../Avatar/Avatar";
import styles from "./CreateEditUser.module.css";
import Input from "../Input/Input";
import RadioInput from "../RadioInput/RadioInput";

const initialUser = {
  first_name: "",
  last_name: "",
  gender: "male",
  dob: "",
  email: "",
  phone: "",
  website: "",
  address: "",
  status: "active",
  avatar: ""
};

class CreateEditUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: initialUser
    };
  }

  componentDidMount() {
    // Если это не страница создания
    if (!this.props.isNew) {
      // Получаем id юзера с роута
      const id = Number(this.props.match.params.id);
      // Если id полученый с роута не равен id юзера с хранилища
      if (id !== Number(this.props.user.id)) {
        // запускаем фунцкцию получения юзера
        this.props.getUser(id);
      } else {
        // или сохраняем в state юзера с хранилища
        this.setState({
          ...this.state,
          user: {
            ...this.props.user,
            avatar: this.props.user._links.avatar.href
          }
        });
      }
    }
  }

  componentDidUpdate(prevProps) {
    // Если это не страница создания юзера и id юзера с prevProps не равен id юзера с текущего props
    if (!this.props.isNew && prevProps.user.id !== this.props.user.id) {
      // то сохраняем в state юзера с хранилища
      this.setState({
        ...this.state,
        user: { ...this.props.user, avatar: this.props.user._links.avatar.href }
      });
    }
  }

  // Функция сохранения изминений данных юзера
  handleUserChange = (name: string, value: string) => {
    this.setState({
      ...this.state,
      user: {
        ...this.state.user,
        [name]: value
      }
    });
  };

  // Функция подтвержения изминений
  onSubmit = () => {
    // Если это страница создания
    if (this.props.isNew) {
      // то возвращаем юзера в state в первоначальное состояние
      this.setState({ ...this.state, user: initialUser });
      // и запускаем функцию создания юзера
      this.props.createUser(this.state.user);
    } else {
      // или запускаем функцию обновления юзера
      this.props.updateUser(this.state.user);
    }
  };

  render() {
    if (this.props.loading) {
      return <div className={styles.loading}>...Loading</div>;
    } else {
      return (
        <div className={styles.container}>
          <form onSubmit={this.onSubmit}>
            <div className={styles.avatar}>
              <Avatar href={this.state.user.avatar} width="200" height="200" />
            </div>
            <div className={styles.indent}>
              <Input
                value={this.state.user.avatar}
                onChange={value => this.handleUserChange("avatar", value)}
                label="Avatar url:"
              />
            </div>
            <div className={styles.indent}>
              <Input
                value={this.state.user.first_name}
                onChange={value => this.handleUserChange("first_name", value)}
                required={true}
                label="First name: "
              />
            </div>
            <div className={styles.indent}>
              <Input
                value={this.state.user.last_name}
                onChange={value => this.handleUserChange("last_name", value)}
                required={true}
                label="Last name: "
              />
            </div>
            <div className={styles.indent}>
              <span className={styles.radioLabel}>Gender:</span>
              <RadioInput
                value="male"
                checked={this.state.user.gender === "male"}
                onChange={value => this.handleUserChange("gender", value)}
              />
              Male
              <RadioInput
                value="female"
                checked={this.state.user.gender === "female"}
                onChange={value => this.handleUserChange("gender", value)}
              />
              Female
            </div>
            <div className={styles.indent}>
              <Input
                value={this.state.user.dob}
                onChange={value => this.handleUserChange("dob", value)}
                label="Dob: "
              />
            </div>
            <div className={styles.indent}>
              <Input
                value={this.state.user.address}
                onChange={value => this.handleUserChange("address", value)}
                label="Address: "
              />
            </div>
            <div className={styles.indent}>
              <Input
                value={this.state.user.phone}
                onChange={value => this.handleUserChange("phone", value)}
                label="Phone: "
              />
            </div>
            <div className={styles.indent}>
              <Input
                value={this.state.user.email}
                onChange={value => this.handleUserChange("email", value)}
                required={true}
                label="Email: "
              />
            </div>
            <div className={styles.indent}>
              <Input
                value={this.state.user.website}
                onChange={value => this.handleUserChange("website", value)}
                label="Website: "
              />
            </div>
            <div className={styles.indent}>
              <span className={styles.radioLabel}>Status:</span>
              <RadioInput
                value="active"
                checked={this.state.user.status === "active"}
                onChange={value => this.handleUserChange("status", value)}
              />
              Active
              <RadioInput
                value="inactive"
                checked={this.state.user.status === "inactive"}
                onChange={value => this.handleUserChange("status", value)}
              />
              Inactive
            </div>
            <div className={styles.buttonContainer}>
              <button className={styles.button} type="submit">
                {this.props.isNew ? "Create" : "Save"}
              </button>
            </div>
          </form>
        </div>
      );
    }
  }
}

CreateEditUser.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    dob: PropTypes.string,
    address: PropTypes.string,
    status: PropTypes.string,
    phone: PropTypes.string,
    website: PropTypes.string
  }).isRequired,
  loading: PropTypes.bool.isRequired,
  getUser: PropTypes.func.isRequired,
  createUser: PropTypes.func.isRequired,
  updateUser: PropTypes.func.isRequired,
  isNew: PropTypes.bool.isRequired,
  match: PropTypes.any,
  history: PropTypes.any
};

export default CreateEditUser;
