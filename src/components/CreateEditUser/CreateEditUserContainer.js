// @flow

import { connect } from "react-redux";
import CreateEditUser from "./CreateEditUser";
import { CREATE_USER, GET_USER, UPDATE_USER } from "../../store/actionTypes";

const mapStateToProps = (state: any, ownProps: any) => ({
  user: state.main.user,
  loading: state.main.loading,
  isNew: ownProps.isNew,
  match: ownProps.match,
  history: ownProps.history
});

const mapDispatchToProps = (dispatch: any) => ({
  getUser: (id: string) => dispatch({ type: GET_USER, payload: id }),
  createUser: data => dispatch({ type: CREATE_USER, payload: data }),
  updateUser: data => dispatch({ type: UPDATE_USER, payload: data })
});

const CreateEditUserContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateEditUser);

export default CreateEditUserContainer;
