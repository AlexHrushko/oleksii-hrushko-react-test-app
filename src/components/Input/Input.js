// @flow

import React from "react";
import styles from "./Input.module.css";

type InputProps = {
  value: string,
  onChange: (value: string) => void,
  required?: boolean,
  label?: string
};

export default function Input(props: InputProps) {
  // Функция отслеживания изминений в инпуте
  function handleChange(event) {
    props.onChange(event.target.value);
  }

  return (
    <label className={styles.label}>
      {props.label}
      <input
        className={styles.input}
        value={props.value || ""}
        required={props.required}
        onChange={event => handleChange(event)}
      />
    </label>
  );
}
