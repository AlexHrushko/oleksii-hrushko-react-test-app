import React from "react";
import { useLocation } from "react-router-dom";
import styles from "./NoMatch.module.css";

export default function NoMatch() {
  let location = useLocation();

  return (
    <div className={styles.container}>
      <h3>
        No match for <code>{location.pathname}</code>
      </h3>
    </div>
  );
}
