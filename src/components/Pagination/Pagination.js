// @flow

import React from "react";
import styles from "./Pagination.module.css";
import {
  FiChevronLeft,
  FiChevronRight,
  FiChevronsLeft,
  FiChevronsRight
} from "react-icons/fi";

type PaginationProps = {
  page: number,
  pageCount: number,
  onClickPrev: () => void,
  onClickNext: () => void,
  onClickAtFirst: () => void,
  onClickAtLast: () => void,
  loading: boolean,
  match: any,
  history: any
};

export default function Pagination(props: PaginationProps) {
  return (
    <div>
      <button
        className={styles.button}
        disabled={props.page === 1 || props.loading}
        onClick={() => props.onClickAtFirst()}
      >
        <FiChevronsLeft />
      </button>
      <button
        className={`${styles.button} ${styles.indentLeft1} ${styles.indentRight2}`}
        disabled={props.page === 1 || props.loading}
        onClick={() => props.onClickPrev()}
      >
        <FiChevronLeft />
      </button>
      <span className={styles.page}>{props.page}</span>
      <button
        className={`${styles.button} ${styles.indentLeft2} ${styles.indentRight1}`}
        disabled={props.page === props.pageCount || props.loading}
        onClick={() => props.onClickNext()}
      >
        <FiChevronRight />
      </button>
      <button
        className={styles.button}
        disabled={props.page === props.pageCount || props.loading}
        onClick={() => props.onClickAtLast()}
      >
        <FiChevronsRight />
      </button>
    </div>
  );
}
