// @flow

import React from "react";
import styles from "./RadioInput.module.css";

type RadioInputProps = {
  value: string,
  onChange: (value: string) => void,
  checked: boolean
};

export default function RadioInput(props: RadioInputProps) {
  // Функция отслеживания изминений в инпуте
  function handleChange(event) {
    props.onChange(event.target.value);
  }

  return (
    <input
      className={styles.input}
      type="radio"
      value={props.value}
      checked={props.checked}
      onChange={event => handleChange(event)}
    />
  );
}
