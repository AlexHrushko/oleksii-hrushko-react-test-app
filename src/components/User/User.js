// @flow

import React, { Component } from "react";
import PropTypes from "prop-types";
import Avatar from "../Avatar/Avatar";
import styles from "./User.module.css";
import { Link } from "react-router-dom";

class User extends Component {
  componentDidMount() {
    const id = Number(this.props.match.params.id);
    this.props.getUser(id);
  }

  render() {
    if (this.props.loading) {
      return <div className={styles.loading}>...Loading</div>;
    } else {
      return (
        <div className={styles.container}>
          <div className={styles.avatar}>
            <Avatar
              href={this.props.user._links.avatar.href}
              width="200"
              height="200"
              alt="avatar"
            />
          </div>
          <div>
            <span>ID:</span>
            {this.props.user.id}
          </div>
          <div>
            <span>First name:</span>
            {this.props.user.first_name}
          </div>
          <div>
            <span>Last name:</span>
            {this.props.user.last_name}
          </div>
          <div>
            <span>Gender:</span>
            {this.props.user.gender}
          </div>
          <div>
            <span>Dob:</span> {this.props.user.dob}
          </div>
          <div>
            <span>Address:</span>
            {this.props.user.address}
          </div>
          <div>
            <span>Phone:</span> {this.props.user.phone}
          </div>
          <div>
            <span>Email:</span> {this.props.user.email}
          </div>
          <div>
            <span>Website:</span>
            {this.props.user.website}
          </div>
          <div>
            <span>Status:</span>
            {this.props.user.status}
          </div>
          <div className={styles.buttonContainer}>
            <Link to={`/users/${this.props.user.id}/edit`}>
              <button className={styles.button}>Edit</button>
            </Link>
          </div>
        </div>
      );
    }
  }
}

User.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    gender: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
    dob: PropTypes.string,
    address: PropTypes.string,
    status: PropTypes.string,
    phone: PropTypes.string,
    website: PropTypes.string
  }).isRequired,
  loading: PropTypes.bool.isRequired,
  getUser: PropTypes.func.isRequired,
  match: PropTypes.any,
  history: PropTypes.any
};

export default User;
