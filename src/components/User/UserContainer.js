// @flow

import { connect } from "react-redux";
import User from "./User";
import { GET_USER } from "../../store/actionTypes";

const mapStateToProps = (state: any, ownProps: any) => ({
  user: state.main.user,
  loading: state.main.loading,
  match: ownProps.match,
  history: ownProps.history
});

const mapDispatchToProps = (dispatch: any) => ({
  getUser: (id: string) => dispatch({ type: GET_USER, payload: id })
});

const UserContainer = connect(mapStateToProps, mapDispatchToProps)(User);

export default UserContainer;
