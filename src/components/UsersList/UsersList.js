// @flow

import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import Avatar from "../Avatar/Avatar";
import styles from "./UsersList.module.css";
import { FiEdit, FiTrash } from "react-icons/fi";

export class UsersList extends Component {
  goToUser = id => {
    this.props.history.push(`/users/${id}`);
  };

  onClickDelete = id => {
    this.props.deleteUser({
      id,
      page: this.props.page
    });
  };

  render() {
    if (this.props.loading) {
      return <div className={styles.container}>...Loading</div>;
    } else {
      return (
        <div className={styles.container}>
          <table>
            <thead>
              <tr>
                <th>Avatar</th>
                <th>Name</th>
                <th className={styles.phone}>Phone</th>
                <th className={styles.email}>Email</th>
                <th className={styles.website}>Website</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {this.props.users.map(user => (
                <tr key={user.id}>
                  <td
                    className={styles.avatar}
                    onClick={() => this.goToUser(user.id)}
                  >
                    <Avatar
                      href={user._links.avatar.href}
                      width="100"
                      height="100"
                    />
                  </td>
                  <td onClick={() => this.goToUser(user.id)}>
                    {user.first_name} {user.last_name}
                  </td>
                  <td
                    className={styles.phone}
                    onClick={() => this.goToUser(user.id)}
                  >
                    {user.phone}
                  </td>
                  <td
                    className={styles.email}
                    onClick={() => this.goToUser(user.id)}
                  >
                    {user.email}
                  </td>
                  <td
                    className={styles.website}
                    onClick={() => this.goToUser(user.id)}
                  >
                    {user.website}
                  </td>
                  <td className={styles.actions}>
                    <Link
                      className={styles.editIcon}
                      to={`/users/${user.id}/edit`}
                    >
                      <FiEdit />
                    </Link>
                    <span
                      className={styles.deleteIcon}
                      onClick={() => this.onClickDelete(user.id)}
                    >
                      <FiTrash />
                    </span>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      );
    }
  }
}

UsersList.propTypes = {
  users: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      first_name: PropTypes.string.isRequired,
      last_name: PropTypes.string.isRequired,
      email: PropTypes.string.isRequired,
      phone: PropTypes.string,
      website: PropTypes.string
    }).isRequired
  ),
  loading: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  deleteUser: PropTypes.func.isRequired,
  match: PropTypes.any,
  history: PropTypes.any
};

export default UsersList;
