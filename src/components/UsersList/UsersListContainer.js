// @flow

import { connect } from "react-redux";
import UsersList from "./UsersList";
import { DELETE_USER } from "../../store/actionTypes";

const mapStateToProps = (state: any, ownProps: any) => ({
  users: state.main.users,
  loading: state.main.loading,
  page: state.main.page,
  match: ownProps.match,
  history: ownProps.history
});

const mapDispatchToProps = (dispatch: any) => ({
  deleteUser: payload => dispatch({ type: DELETE_USER, payload })
});

const UsersListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersList);

export default UsersListContainer;
