// @flow

import React, { Component } from "react";
import PropTypes from "prop-types";
import UsersListContainer from "../UsersList/UsersListContainer";
import Pagination from "../Pagination/Pagination";
import { Link } from "react-router-dom";
import styles from "./UsersPage.module.css";
import { FiPlus } from "react-icons/fi";

class UsersPage extends Component {
  componentDidMount() {
    const page = Number(this.props.match.params.page);
    if (page) {
      this.props.setPage(page);
      this.props.getUsers(page);
    } else {
      this.props.getUsers(this.props.page);
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.page !== prevProps.page) {
      this.props.getUsers(this.props.page);
    }
  }

  // Функция обработки нажатия на кнопку prev в пагинации
  onClickPrev = () => {
    if (this.props.page === 2) {
      this.props.history.push("/");
      this.props.setPage(1);
    } else {
      this.props.history.push(`/page/${this.props.page - 1}`);
      this.props.setPage(this.props.page - 1);
    }
  };

  // Функция обработки нажатия на кнопку next в пагинации
  onClickNext = () => {
    this.props.history.push(`/page/${this.props.page + 1}`);
    this.props.setPage(this.props.page + 1);
  };

  // Функция обработки нажатия на кнопку at first в пагинации
  onClickAtFirst = () => {
    this.props.history.push("/");
    this.props.setPage(1);
  };

  // Функция обработки нажатия на кнопку at last в пагинации
  onClickAtLast = () => {
    this.props.history.push(`/page/${this.props.pageCount}`);
    this.props.setPage(this.props.pageCount);
  };

  render() {
    return (
      <div>
        <div className={styles.inline}>
          <Pagination
            page={this.props.page}
            pageCount={this.props.pageCount}
            match={this.props.match}
            history={this.props.history}
            onClickPrev={this.onClickPrev}
            onClickNext={this.onClickNext}
            onClickAtFirst={this.onClickAtFirst}
            onClickAtLast={this.onClickAtLast}
            loading={this.props.loading}
          />
        </div>
        <div className={styles.inlineRight}>
          <Link to={"/users/new"}>
            <button className={styles.button}>
              <FiPlus />
            </button>
          </Link>
        </div>
        <UsersListContainer
          match={this.props.match}
          history={this.props.history}
        />
        <div className={styles.inline}>
          <Pagination
            page={this.props.page}
            pageCount={this.props.pageCount}
            match={this.props.match}
            history={this.props.history}
            onClickPrev={this.onClickPrev}
            onClickNext={this.onClickNext}
            onClickAtFirst={this.onClickAtFirst}
            onClickAtLast={this.onClickAtLast}
            loading={this.props.loading}
          />
        </div>
        <div className={styles.inlineRight}>
          <Link to={"/users/new"}>
            <button className={styles.button}>
              <FiPlus />
            </button>
          </Link>
        </div>
      </div>
    );
  }
}

UsersPage.propTypes = {
  page: PropTypes.number.isRequired,
  pageCount: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
  getUsers: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  match: PropTypes.any,
  history: PropTypes.any
};

export default UsersPage;
