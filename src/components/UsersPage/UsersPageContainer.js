// @flow

import { connect } from "react-redux";
import { GET_USERS, SET_PAGE } from "../../store/actionTypes";
import UsersPage from "./UsersPage";

const mapStateToProps = (state: any, ownProps: any) => ({
  page: state.main.page,
  pageCount: state.main.pageCount,
  loading: state.main.loading,
  match: ownProps.match,
  history: ownProps.history
});

const mapDispatchToProps = (dispatch: any) => ({
  setPage: (page: number) => dispatch({ type: SET_PAGE, payload: page }),
  getUsers: (page: number) => dispatch({ type: GET_USERS, payload: page })
});

const UsersPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(UsersPage);

export default UsersPageContainer;
