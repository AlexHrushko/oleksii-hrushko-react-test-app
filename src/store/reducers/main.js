// @flow

import {
  CREATE_USER_FAILED,
  CREATE_USER_SUCCEEDED,
  DELETE_USER_FAILED,
  DELETE_USER_SUCCEEDED,
  GET_USER_FAILED,
  GET_USER_SUCCEEDED,
  GET_USERS_FAILED,
  GET_USERS_SUCCEEDED,
  SET_LOADING,
  SET_PAGE,
  UPDATE_USER_FAILED,
  UPDATE_USER_SUCCEEDED
} from "../actionTypes";

const initialState = {
  users: [],
  page: 1,
  pageCount: 1,
  loading: false,
  user: {
    id: "",
    first_name: "",
    last_name: "",
    gender: "",
    dob: "",
    email: "",
    phone: "",
    website: "",
    address: "",
    status: "",
    _links: {
      avatar: {
        href: ""
      }
    }
  }
};

const main = (state = initialState, action: { type: string, payload: any }) => {
  switch (action.type) {
    case GET_USERS_SUCCEEDED:
      return {
        ...state,
        users: action.payload.data,
        pageCount: action.payload.pageCount,
        loading: initialState.loading
      };
    case GET_USERS_FAILED:
      return {
        ...state,
        users: initialState.users,
        loading: initialState.loading
      };
    case SET_PAGE:
      return {
        ...state,
        page: action.payload
      };
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case GET_USER_SUCCEEDED:
      return {
        ...state,
        user: action.payload,
        loading: initialState.loading
      };
    case GET_USER_FAILED:
      return {
        ...state,
        user: initialState.user,
        loading: initialState.loading
      };
    case UPDATE_USER_SUCCEEDED:
      return {
        ...state,
        user: action.payload,
        loading: initialState.loading
      };
    case UPDATE_USER_FAILED:
      return {
        ...state,
        loading: initialState.loading
      };
    case DELETE_USER_SUCCEEDED:
      return {
        ...state,
        loading: initialState.loading
      };
    case DELETE_USER_FAILED:
      return {
        ...state,
        loading: initialState.loading
      };
    case CREATE_USER_SUCCEEDED:
      return {
        ...state,
        loading: initialState.loading
      };
    case CREATE_USER_FAILED:
      return {
        ...state,
        loading: initialState.loading
      };
    default:
      return state;
  }
};

export default main;
