// @flow

import { put, takeEvery, call } from "redux-saga/effects";
import {
  CREATE_USER,
  CREATE_USER_FAILED,
  CREATE_USER_SUCCEEDED,
  DELETE_USER,
  DELETE_USER_FAILED,
  GET_USER,
  GET_USER_FAILED,
  GET_USER_SUCCEEDED,
  GET_USERS,
  GET_USERS_FAILED,
  GET_USERS_SUCCEEDED,
  SET_LOADING,
  UPDATE_USER,
  UPDATE_USER_FAILED,
  UPDATE_USER_SUCCEEDED
} from "../actionTypes";
import { service } from "../service";

function* getUsers(action) {
  try {
    yield put({ type: SET_LOADING, payload: true });
    const response = yield call(service.getUsers, action.payload);
    if (response.success) {
      yield put({ type: GET_USERS_SUCCEEDED, payload: response.data });
    } else {
      yield put({ type: GET_USERS_FAILED });
    }
  } catch (e) {
    yield put({ type: GET_USERS_FAILED });
  }
}

function* getUser(action) {
  try {
    yield put({ type: SET_LOADING, payload: true });
    const response = yield call(service.getUser, action.payload);
    if (response.success) {
      yield put({ type: GET_USER_SUCCEEDED, payload: response.data });
    } else {
      yield put({ type: GET_USER_FAILED });
    }
  } catch (e) {
    yield put({ type: GET_USER_FAILED });
  }
}

function* updateUser(action) {
  try {
    yield put({ type: SET_LOADING, payload: true });
    const response = yield call(service.updateUser, action.payload);
    if (response.success) {
      yield put({ type: UPDATE_USER_SUCCEEDED, payload: response.data });
    } else {
      yield put({ type: UPDATE_USER_FAILED });
    }
  } catch (e) {
    yield put({ type: UPDATE_USER_FAILED });
  }
}

function* deleteUser(action) {
  try {
    yield put({ type: SET_LOADING, payload: true });
    const deleteUserResponse = yield call(
      service.deleteUser,
      action.payload.id
    );
    // Если удаление юзера пройшоло успешно
    if (deleteUserResponse.success) {
      // то запускаем запрос на получение всех юзеров на нужной странице
      const getUsersResponse = yield call(
        service.getUsers,
        action.payload.page
      );
      if (getUsersResponse.success) {
        yield put({
          type: GET_USERS_SUCCEEDED,
          payload: getUsersResponse.data
        });
      } else {
        yield put({ type: GET_USERS_FAILED });
      }
    } else {
      yield put({ type: DELETE_USER_FAILED });
    }
  } catch (e) {
    yield put({ type: DELETE_USER_FAILED });
  }
}

function* createUser(action) {
  try {
    yield put({ type: SET_LOADING, payload: true });
    const response = yield call(service.createUser, action.payload);
    if (response.success) {
      yield put({ type: CREATE_USER_SUCCEEDED });
    } else {
      yield put({ type: CREATE_USER_FAILED });
    }
  } catch (e) {
    yield put({ type: CREATE_USER_FAILED });
  }
}

export default function* rootSaga() {
  yield takeEvery(GET_USERS, getUsers);
  yield takeEvery(GET_USER, getUser);
  yield takeEvery(UPDATE_USER, updateUser);
  yield takeEvery(DELETE_USER, deleteUser);
  yield takeEvery(CREATE_USER, createUser);
}
