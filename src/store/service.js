// @flow

import axios from "axios";

export const service = {
  getUsers,
  getUser,
  updateUser,
  deleteUser,
  createUser
};

const baseRestUrl = `https://gorest.co.in/public-api/`;
const accessToken = "Bearer UYExMuV7BP_eLhq0Hevl35_ypd8RShAwZtPa";

// Функция для получения всех пользователей на нужной странице
function getUsers(page: number) {
  return axios({
    method: "get",
    baseURL: baseRestUrl,
    url: `/users?page=${page}`,
    headers: {
      Authorization: accessToken
    }
  })
    .then(function(response) {
      return {
        success: response.data._meta.success,
        data: {
          data: response.data.result,
          pageCount: response.data._meta.pageCount
        }
      };
    })
    .catch(function(error) {
      console.log(error);
      return {
        success: false
      };
    });
}

// Функция для получения конкретного пользователя
function getUser(id: string) {
  return axios({
    method: "get",
    baseURL: baseRestUrl,
    url: `/users/${id}`,
    headers: {
      Authorization: accessToken
    }
  })
    .then(function(response) {
      return {
        success: response.data._meta.success,
        data: response.data.result
      };
    })
    .catch(function(error) {
      console.log(error);
      return {
        success: false
      };
    });
}

// Функция для обновления пользователя
function updateUser(data: any) {
  return axios({
    method: "put",
    baseURL: baseRestUrl,
    url: `/users/${data.id}`,
    headers: {
      Authorization: accessToken
    },
    data
  })
    .then(function(response) {
      return {
        success: response.data._meta.success,
        data: response.data.result
      };
    })
    .catch(function(error) {
      console.log(error);
      return {
        success: false
      };
    });
}

// Функция для удаления пользователя
function deleteUser(id: string) {
  return axios({
    method: "delete",
    baseURL: baseRestUrl,
    url: `/users/${id}`,
    headers: {
      Authorization: accessToken
    }
  })
    .then(function(response) {
      return {
        success: response.data._meta.success
      };
    })
    .catch(function(error) {
      console.log(error);
      return {
        success: false
      };
    });
}

// Функция для создания пользователя
function createUser(data: any) {
  return axios({
    method: "post",
    baseURL: baseRestUrl,
    url: `/users`,
    headers: {
      Authorization: accessToken
    },
    data
  })
    .then(function(response) {
      return {
        success: response.data._meta.success
      };
    })
    .catch(function(error) {
      console.log(error);
      return {
        success: false
      };
    });
}
